const HTTP = require("http");
const PORT = 4000;

HTTP.createServer((req, res) => {
    if(req.url == "/" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("Welcome to booking system.");
    } else if(req.url == "/profile" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("Welcome to your profile.");
    } else if(req.url == "/courses" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("Here's our courses available.");
    } else if(req.url == "/addCourse" && req.method == "POST"){
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("Add course to our resources.");
    } else if(req.url == "/updateCourse" && req.method == "PUT"){
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("Update a course to our resources.");
    } else if(req.url == "/archiveCourse" && req.method == "DELETE"){
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("Archive course to our resources.");
    } else {
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.end("Page not found.");
    }
}).listen(PORT, console.log(`Listening to port ${PORT}`))